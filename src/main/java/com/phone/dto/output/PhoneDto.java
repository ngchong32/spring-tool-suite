package com.phone.dto.output;

import com.phone.model.Brand;
import com.phone.model.Phone;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PhoneDto {
    private long id;
    private String phoneName;
    private Brand brand;

    public PhoneDto(Phone phone) {
        this.id = phone.getId();
        this.phoneName = phone.getPhoneName();
        this.brand = phone.getBrand();
    }

}
