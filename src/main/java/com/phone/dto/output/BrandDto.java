package com.phone.dto.output;

import com.phone.model.Brand;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BrandDto {
    private Long id;

    private String brandName;

    public BrandDto(Brand brand) {
        this.id = brand.getId();
        this.brandName = brand.getBrandName();
    }

}
