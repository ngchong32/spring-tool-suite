package com.phone.dto.output;

import java.util.Date;

import com.phone.model.Detail;
import com.phone.model.Phone;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DetailDto {
    private long id;
    private Phone phone;
    private int ram;
    private int cpu;
    private String address;
    private Date ngaySX;
    private String color;

    public DetailDto(Detail detail) {
        this.id = detail.getId();
        this.phone = detail.getPhone();
        this.ram = detail.getRam();
        this.cpu = detail.getCpu();
        this.ngaySX = detail.getNgaySX();
        this.color = detail.getColor();
    }

}
