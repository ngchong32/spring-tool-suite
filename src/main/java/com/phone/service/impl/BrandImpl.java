package com.phone.service.impl;

import com.phone.dto.output.BrandDto;
import com.phone.model.Brand;
import com.phone.repository.BrandRepository;
import com.phone.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BrandImpl implements BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public List<Brand> searchBrand() {
        return brandRepository.findAll();
    }

    @Override
    public BrandDto createBranch(BrandDto brandDto) {
        Brand brand = new Brand();
        brand.setId(brandDto.getId());
        brand.setBrandName(brandDto.getBrandName());
        brandRepository.save(brand);
        return brandDto;
    }

    @Override
    public BrandDto deleteBranch(Long id) {
        Optional<Brand> brand = brandRepository.findById(id);
        BrandDto brandDto = new BrandDto();
        brandDto.setId(brand.get().getId());
        brandDto.setBrandName(brand.get().getBrandName());
        if (brand != null) {
            brandRepository.delete(brand.get());
            return brandDto;
        }
        return null;
    }


  /*  @Override
    public List<PhoneDto> findPhoneByBrandnYear(String brand, int year) {
        List<PhoneDto> phoneDtos = new ArrayList<>();
        for (Phone phone: phoneRepository.findByBrandnYear(brand, year)){
            PhoneDto phoneDto=new PhoneDto(phone);
            phoneDtos.add(phoneDto);
        }
        return phoneDtos*/
}
