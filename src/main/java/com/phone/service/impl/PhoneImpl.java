//package com.phone.service.impl;
//
//import com.phone.dto.output.PhoneDto;
//import com.phone.model.Phone;
//import com.phone.repository.PhoneRepository;
//import com.phone.service.PhoneService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//@Service
//public class PhoneImpl implements PhoneService {
//
//    @Autowired
//    private PhoneRepository phoneRepository;
//
//    @Override
//    public List<PhoneDto> findPhoneByBrandnYear(String brand, int year) {
//        List<PhoneDto> phoneDtos = new ArrayList<>();
//        for (Phone phone: phoneRepository.findByBrandnYear(brand, year)){
//            PhoneDto phoneDto=new PhoneDto(phone);
//            phoneDtos.add(phoneDto);
//        }
//        return phoneDtos;
//    }
//
//    @Override
//    public List<PhoneDto> findPhoneByColor(String color) {
//        List<PhoneDto> phoneDtos =new ArrayList<>();
//        for(Phone phone: phoneRepository.findPhoneByColor(color)){
//            PhoneDto phoneDto=new PhoneDto(phone);
//            phoneDtos.add(phoneDto);
//        }
//        return phoneDtos;
//    }
//
//    @Override
//    public List<PhoneDto> findPhoneByAddressnBrandName(String add, String brand) {
//        List<PhoneDto> phoneDtos = new ArrayList<>();
//        for(Phone phone: phoneRepository.findByAddressnBrandName(add,brand)){
//            PhoneDto phoneDto=new PhoneDto(phone);
//            phoneDtos.add(phoneDto);
//        }
//        return phoneDtos;
//    }
//}
