package com.phone.service;

import com.phone.dto.output.BrandDto;
import com.phone.model.Brand;

import java.util.List;

public interface BrandService {
    List<Brand> searchBrand();

    BrandDto createBranch(BrandDto brandDto);

    BrandDto deleteBranch(Long id);
}
