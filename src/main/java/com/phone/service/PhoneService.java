package com.phone.service;

import com.phone.dto.output.PhoneDto;

import java.util.List;

public interface PhoneService {
    List<PhoneDto> findPhoneByBrandnYear(String brand, int year);

    List<PhoneDto> findPhoneByColor(String color);

    List<PhoneDto> findPhoneByAddressnBrandName(String add, String brand);
}
