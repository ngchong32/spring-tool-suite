package com.phone.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "Brand")
@NoArgsConstructor
public class Brand {
    @Id
    private Long id;

    @Column(name = "BrandName")
    private String brandName;

}
