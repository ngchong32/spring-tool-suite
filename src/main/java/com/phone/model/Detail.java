package com.phone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Detail")
@NoArgsConstructor
public class Detail {
    @Id
    private Long id;

    @ManyToOne
    @JoinColumn(name = "PhoneId")
    private Phone phone;

    @Column(name = "Ram")
    private int ram;

    @Column(name ="Cpu")
    private int cpu;

    @Column(name = "Address")
    private String address;

    @Column(name = "NgaySanXuat")
    private Date ngaySX;

    @Column(name = "Color")
    private String color;

}
