package com.phone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Phone")
@NoArgsConstructor

public class Phone {
    @Id
    private Long id;

    @Column(name = "PhoneName")
    private String phoneName;

    @ManyToOne
    @JoinColumn(name = "BrandId")
    private Brand brand;


}
