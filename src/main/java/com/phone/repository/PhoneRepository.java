package com.phone.repository;

import com.phone.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhoneRepository extends JpaRepository<Phone, Long> {
//    //tìm theo color
//    @Query(value = "select * from Phone p inner join Detail d on p.phoneId=d.phoneId where d.color like %:keyColer% ", nativeQuery = true)
//    List<Phone> findPhoneByColor(@Param("keyColor") String keyColor);
//
//    //tìm kiếm theo tên hãng và năm>year
//    @Query(value = "select * from((Phone p inner join Detail d on p.id=d.phone) inner join Brand b on p.brand=b.id) where b.brandNam like %:keyBrandName% and Year(d.ngaySX)> year", nativeQuery = true)
//    List<Phone> findByBrandnYear(@Param("keyBrandName") String keyBrandName, int year);
//
//    //Tìm kiếm theo địa chỉ và tên hãng
//    @Query(value = "select * from((Phone p inner join Detail d on p.id=d.phone) inner join Brand b on p.brand=b.id) where d.address like %:keyAddress% and b.brandName like %:keyBrandName%", nativeQuery = true)
//    List<Phone> findByAddressnBrandName(@Param("keyAddress") String keyAddress, @Param("keyBrandName") String keyBrandName);


}
