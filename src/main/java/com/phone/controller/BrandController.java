package com.phone.controller;

import com.phone.dto.output.BrandDto;
import com.phone.model.Brand;
import com.phone.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brand")

public class BrandController {
    @Autowired
    private BrandService brandService;

    @GetMapping("/allBrand")
    private List<Brand> getBrand() {
        List<Brand> brands = brandService.searchBrand();
        return brands;
    }

    @PostMapping("/createBrand")
    private BrandDto createBrand(@RequestBody BrandDto brandDto){
        return brandService.createBranch(brandDto);
    }

    @DeleteMapping("/deleteBrand")
    private BrandDto deteleBrand(@RequestParam Long id){
        return brandService.deleteBranch(id);
    }
}
