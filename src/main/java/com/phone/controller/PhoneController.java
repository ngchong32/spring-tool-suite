//package com.phone.controller;
//
//import com.phone.dto.output.PhoneDto;
//import com.phone.service.PhoneService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/phone")
//public class PhoneController {
//    @Autowired
//    private PhoneService phoneService;
//
//    @GetMapping("/byColor")
//    private List<PhoneDto> getByColor(@RequestParam String color) {
//        List<PhoneDto> phoneDtos = phoneService.findPhoneByColor(color);
//        return phoneDtos;
//    }
//
//    @GetMapping("/byBrandnYear")
//    private  List<PhoneDto> getByBrandnYear(@RequestParam String brand, int year){
//        List<PhoneDto> phoneDtos=phoneService.findPhoneByBrandnYear(brand,year);
//        return  phoneDtos;
//    }
//
//    @GetMapping("/byAddressnBrandName")
//    private  List<PhoneDto> getByAddressnBrandName(@RequestParam String address, String brandName){
//        List<PhoneDto> phoneDtos=phoneService.findPhoneByAddressnBrandName(address,brandName);
//        return  phoneDtos;
//    }
//
//}
